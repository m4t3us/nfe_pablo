from django.db import models
from simple_history.models import HistoricalRecords

# Create your models here.
class Servico(models.Model):
    cruva_choices = (
        (0, 'A'),
        (1, 'B'),
        (2, 'C'),
    )
    palavra_chave = models.CharField(max_length=40)
    descricao = models.CharField(max_length=140)
    tipo_de_servico = models.CharField(max_length=140)
    curva = models.PositiveSmallIntegerField(null=True, blank=True, choices=cruva_choices)
    history = HistoricalRecords()

    def __str__(self):
        return self.descricao
