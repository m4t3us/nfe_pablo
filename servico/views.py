from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.db.models import Count
from .models import Servico
import json




class ServicoView:

    @login_required
    def listTemplate(request, codigo=None):
        context = { 'template' : 'Serviços',}
        return render( request , 'list-servico.html' , context )

    @login_required
    def list(request, codigo=None):
        if codigo:
            pass
        else:
            servicos = Servico.objects.all().order_by('curva').values()
            servicos = list(servicos)
            return JsonResponse( servicos, safe=False  )

    @login_required
    def add(request):
        servico_new = json.loads(request.body)
        servico = Servico()
        servico.descricao = servico_new.get('descricao')
        servico.palavra_chave = servico_new.get('palavra_chave')
        servico.tipo_de_servico = servico_new.get('tipo_de_servico')
        servico.curva = servico_new.get('curva')
        servico.save()

        return JsonResponse( {'sucesso':True}, safe=False  )

    @login_required
    def edit(request):
        servico_new = json.loads(request.body)
        servico = Servico.objects.get(pk=servico_new.get('id'))
        servico.descricao = servico_new.get('descricao')
        servico.palavra_chave = servico_new.get('palavra_chave')
        servico.tipo_de_servico = servico_new.get('tipo_de_servico')
        servico.curva = servico_new.get('curva')
        servico.save()

        return JsonResponse( {'sucesso':True}, safe=False  )
        
  