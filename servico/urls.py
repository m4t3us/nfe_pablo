from django.urls import path, re_path
from .views import ServicoView

urlpatterns = [
    path('', ServicoView.listTemplate , name='servico_list_template'),
    path('list', ServicoView.list , name='servico_list'),
    path('add', ServicoView.add , name='servico_add'),
    path('edit', ServicoView.edit , name='servico_edit'),
]

