//const endPoint = 'http://localhost:8000'
const endPoint = 'https://nfe-estagio.herokuapp.com'
app.controller('ServicoController', function($scope, $http, $window, $localStorage){
    const init = function(){
        console.log('Controller' , 'Servicos')
        if(!$localStorage.obra)  $window.location.href = "/obra/selecionar_obra"
        $scope.getServicos()
    }
   
    $scope.getServicos = function(){
        $http.get(`${endPoint}/servico/list`).then(
            function(data){
               $scope.servicos = data.data
               console.log($scope.servicos)
            },
            function(err){
                console.log(err)
            }
        )
    }

    $scope.addServico = function(){
        console.log($scope.servico)
        $http.post(`${endPoint}/servico/add`, $scope.servico).then(
            function(data){
              console.log(data)
              $scope.alert = {
                sucesso:true,
                msg:`Serviço ${$scope.servico.descricao} cadastrado com suscesso`
              }
              delete $scope.servico
              $scope.getServicos()
            },
            function(err){
                console.log(err)
                $scope.alert = {
                    sucesso:false,
                    msg:'Não foi possível cadastrar o serviço'
                }
            }
        )
    }

    $scope.abrirModalEditar = function(servico){
        console.log(servico)
        delete $scope.alert_editar
        servico.curva = servico.curva + ''
        $scope.servico_editar = servico  
        $("#modalEditarServico").modal("show");
    }

    $scope.fecharModalAlterar = function(){
        $scope.formAlterar.$setPristine()
        $scope.getServicos()
    }

    $scope.alterarServico = function(){
        console.log($scope.servico_editar)
        $http.post(`${endPoint}/servico/edit`, $scope.servico_editar).then(
            function(data){
              console.log(data)
              $scope.alert_editar = {
                sucesso:true,
                msg:`Serviço ${$scope.servico_editar.descricao} alterado com suscesso`
              }
              //$scope.getServicos()
            },
            function(err){
                console.log(err)
                $scope.alert_editar = {
                    sucesso:false,
                    msg:'Não foi possível alterar o serviço'
                }
            }
        )
    }

    init()
})


app.controller('NfeController' , function($scope,$http, $window, $localStorage){
    const init = function(){
        console.log('Controller' , 'Nfe')
        if(!$localStorage.obra)  $window.location.href = "/obra/selecionar_obra"
        $scope.getNfe()
    }
    

    $scope.isLoading = false

    $scope.getNfe = function(){
        $http.get(`${endPoint}/nfe/list`).then(
            function(data){
               $scope.nfe = data.data
               //if($scope.nfe) $scope.nfe = $scope.nfe.sort(function(a, b){return a.curva - b.curva});
               console.log($scope.nfe)
            },
            function(err){
                console.log(err)
            }
        )
    }
 
    $scope.importarNfe = function(){
        var fd = new FormData();
        fd.append('nfe', $scope.fileArray[0]);
        $scope.isLoading = true
        $http({
            method: 'POST',
            url: `${endPoint}/nfe/lerNfe`,
            headers: {
              'Content-Type': undefined
            },
            data: fd,
            transformRequest: angular.identity
        })
        .then(function (response) {
          console.log(response.data.msg)
          delete $scope.fileArray
          $('#filenfe').val('');
          $scope.isLoading = false
          $scope.alert = {
              sucesso:true,
              msg:response.data.msg
          }
          $scope.getNfe()
        }, function(err){ 
            console.log(err.data.msg) 
            delete $scope.fileArray
            $scope.isLoading = false
            $('#filenfe').val('');
            $scope.alert = {
                sucesso:false,
                msg:err.data.msg
            }
        })
    }

   

    init()
})


app.controller('ObraController' , function($scope,$http,$localStorage,$window){
    

    const init = function(){
        console.log('Controller' , 'Obras')
        //if($localStorage.obra) $window.location.href = "/nfe/"
        $scope.getObras()
    }

    $scope.teste = function(){
        alert('entrou')
    }
    
    $scope.getObras = function(){
        $http.get(`${endPoint}/obra/list`).then(
            function(data){
               $scope.obras = data.data
               console.log($scope.obras)
            },
            function(err){
                console.log(err)
            }
        )
    }

    $scope.selecionarObra = function(obra){
        $scope.obra = obra
    }

    $scope.confirmarSelecao = function(){
        $localStorage.obra = $scope.obra
        console.log($localStorage.obra)
        $window.location.href = "/nfe/"
    }

    init()
})


app.controller('ObraSelecionadaController' , function($scope,$http,$localStorage,$window){
    
    const init = function(){
        console.log('Controller' , 'ObraSelecionada')
        //if($localStorage.obra) $window.location.href = "/nfe/"
        if($localStorage.obra)  $scope.obra = $localStorage.obra
       
    }

    init()
    
})


app.controller('LogoutController' , function($scope,$http,$localStorage,$window){
    
    const init = function(){
        console.log('Controller' , 'Logout')
    }

    $scope.logout = function(){
        delete $localStorage.obra
    }

    init()
    
})


app.controller('RelatorioController' , function($scope,$http,$localStorage,$window){
    $scope.graficoMensal = null
    $scope.graficoGastoPorServico = null
    $scope.graficoGastoPorMaterial = null

    const init = function(){
        console.log('Controller' , 'Relatorios')
        if($localStorage.obra)  $scope.obra = $localStorage.obra
        getGastoMensal()
        getGastoPorServico()
        getGastoPorMaterial()
    }

    const getGastoPorMaterial = function(){
        $http.get(`${endPoint}/relatorio/list_gasto_por_material`).then(
            function(data){
               gastos = data.data

               $scope.graficoGastoPorMaterial = {
                    "chart": {
                        "caption": "Gasto por Material [2019]",
                        "subCaption": "",
                        "xAxisName": "",
                        "yAxisName": "Valores em R$",
                        "numberSuffix": "",
                        "theme": "fusion",
                        "yAxisValueDecimals": "2",
                        "xAxisValueDecimals": "3",
                        //"formatNumber": "0",
                        "showlegend": "1",
                        "showpercentvalues": "1",
                        "legendposition": "bottom",
                        "usedataplotcolorforlabels": "1",
                    },
                    "data": []
                };
                gastos.forEach(element => {
                    $scope.graficoGastoPorMaterial.data.push( {
                        label: `${element.descricao.substr(0,15)}...`,
                        value: element.sum_score
                    })
                });

                console.log('Grafico Gasto por Material' , $scope.graficoGastoPorMaterial)

            },
            function(err){
                console.log(err)
            }
        )
    }

    const getGastoPorServico = function(){
        $http.get(`${endPoint}/relatorio/list_gasto_por_servico`).then(
            function(data){
               gastos = data.data

               $scope.graficoGastoPorServico = {
                    "chart": {
                        "caption": "Gasto por Serviço [2019]",
                        "subCaption": "",
                        "xAxisName": "",
                        "yAxisName": "Valores em R$",
                        "numberSuffix": "",
                        "theme": "fusion",
                        "yAxisValueDecimals": "2",
                        "xAxisValueDecimals": "3",
                        //"formatNumber": "0",
                        "showlegend": "1",
                        "showpercentvalues": "1",
                        "legendposition": "bottom",
                        "usedataplotcolorforlabels": "1",
                    },
                    "data": []
                };
                gastos.forEach(element => {
                    $scope.graficoGastoPorServico.data.push( {
                        label: `${element.servico.substr(0,15)}..`,
                        value: element.sum_score
                    })
                });

                console.log('Grafico Gasto por Serviço' , $scope.graficoGastoPorServico)

            },
            function(err){
                console.log(err)
            }
        )
    }

    const getGastoMensal = function(){
        $http.get(`${endPoint}/relatorio/list_gasto_mensal`).then(
            function(data){
               gastos = data.data

               $scope.graficoMensal = {
                    "chart": {
                        "caption": "Gasto Mensal [2019]",
                        "subCaption": "",
                        "xAxisName": "",
                        "yAxisName": "Valores em R$",
                        "numberSuffix": "",
                        "theme": "fusion",
                        "yAxisValueDecimals": "2",
                        "xAxisValueDecimals": "3",
                       //"formatNumber": "0",
                      
                    },
                    "data": []
                };
                gastos.forEach(element => {
                    $scope.graficoMensal.data.push( {
                        label: `${element.data__month}/${element.data__year}`,
                        value: element.sum_score
                    })
                });

                console.log('Grafico Gasto Mensal' , $scope.graficoMensal)

            },
            function(err){
                console.log(err)
            }
        )
    }


   

    init()
    
})
