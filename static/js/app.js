
const app = angular.module('SistemaNfe',[
       'ngCookies',
       'ngStorage',
       'ng-fusioncharts',
    ], function($interpolateProvider, $httpProvider){
       // Contorna prroblema de interpolação da renderização de template do django
      
       $interpolateProvider.startSymbol('{[{');
       $interpolateProvider.endSymbol('}]}');
       $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
       $httpProvider.defaults.xsrfCookieName = 'csrftoken';
       $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    })
    /*.run( function run($http, $cookies ){
       // Evita problemas relacionados ao CSRF
       $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    })*/
    .directive("selectNgFiles", function() {
      return {
        require: "ngModel",
        link: function postLink(scope,elem,attrs,ngModel) {
          elem.on("change", function(e) {
            var files = elem[0].files;
            ngModel.$setViewValue(files);
          })
        }
      }
    })