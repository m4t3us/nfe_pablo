from django.urls import path, re_path
from .views import RelatorioView

urlpatterns = [
    path('', RelatorioView.listTemplate , name='relatorio_template_list'),
    path('list_gasto_mensal', RelatorioView.list_gasto_mensal , name='relatorio_list_gasto_mensal'),
    path('list_gasto_por_servico', RelatorioView.list_gasto_por_servico , name='relatorio_list_gasto_por_servico'),
    path('list_gasto_por_material', RelatorioView.list_gasto_por_material , name='relatorio_list_gasto_por_material'),
]

