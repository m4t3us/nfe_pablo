from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.db.models import Count, Sum
from django.conf import settings

from  servico.models import Servico
from  nfe.models import Nfe


class RelatorioView:

    @login_required
    def listTemplate(request,):
        context = { 'template' : 'Relatórios' }
        return render( request , 'relatorio-nfe.html' , context )

    @login_required
    def list_gasto_mensal(request, ):
        nfe = Nfe.objects.values('data__year', 'data__month').annotate(sum_score=Sum('valor_total_nota')).order_by('data__year','data__month') 
        return JsonResponse( list(nfe), safe=False  )

    @login_required
    def list_gasto_por_servico(request, ):
        nfe = Nfe.objects.values('servico').annotate(sum_score=Sum('valor_total_nota'))
        nfe = list(nfe)
        for n in nfe:
            n['servico'] = list(Servico.objects.filter(id=n['servico']).values('tipo_de_servico'))[0]['tipo_de_servico'] 
        return JsonResponse( nfe, safe=False  )

    @login_required
    def list_gasto_por_material(request, ):
        nfe = Nfe.objects.values('descricao').annotate(sum_score=Sum('valor_total_nota'))
        return JsonResponse( list(nfe), safe=False  )