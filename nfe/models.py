from django.db import models
from simple_history.models import HistoricalRecords
from servico.models import Servico
# Create your models here.


class Nfe(models.Model):
    numero = models.IntegerField(unique=True,db_index=False)
    data =  models.DateField() 
    remetente = models.CharField(max_length=240, null=True, blank=True)
    quantidade = models.CharField(max_length=140)
    descricao = models.CharField(max_length=500, null=True, blank=True)
    valor_unitario = models.DecimalField(max_digits=50,decimal_places=2)
    valor_total = models.DecimalField(max_digits=50,decimal_places=2, null=True, blank=True)
    valor_total_nota = models.DecimalField(max_digits=50,decimal_places=2)
    servico = models.ForeignKey(Servico, on_delete=models.CASCADE)
    pdf_dir = models.CharField(max_length=140, null=True, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.servico.descricao 



