from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.db.models import Count
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from .models import Nfe
from  servico.models import Servico
import os
from datetime import *
import datetime
import PyPDF2
from django.core import serializers
from django.contrib.auth.decorators import login_required, permission_required

class NfeView:

    @login_required
    def listTemplate(request,):
        context = { 'template' : 'Notas Fiscais' }
        return render( request , 'list-nfe.html' , context )

    @login_required
    def list(request, ):
        nfe = Nfe.objects.all().values()
       
        nfe = list(nfe)
        for n in nfe:
            n['servico'] = list(Servico.objects.filter(id=n['servico_id']).values('tipo_de_servico')) 
       
        return JsonResponse( nfe, safe=False  )

    @login_required
    def lerNfe(request, ):
        try:
            #upload nfe
            myfile = request.FILES['nfe']
            fs = FileSystemStorage()
            filename = fs.save( datetime.datetime.now().strftime("%Y%m%d_%H%M%S") + '.pdf', myfile)
            uploaded_file_url = fs.url(filename)
            
            #ler nfe
            pdf_file = open( settings.MEDIA_ROOT +"/"+ filename , 'rb')
            read_pdf = PyPDF2.PdfFileReader(pdf_file)
            number_of_pages = read_pdf.getNumPages()

            #busca informações imporantes da nfe
            count = number_of_pages
            i = 0
            numero = data_emissao = quantidade = valor_total_nota = valor_unitario = descricao = None
            for i in range(count):
                page = read_pdf.getPage(i)
                nota = page.extractText()
                if i == 0:
                    
                    nota = nota.split('\n')
                    #print(nota)
                    for index,n in enumerate(nota):
                        if 'Nº' in n and not numero:
                            numero = n.split(' ')[1].strip()
                            print('Numero' , numero)
                        if 'DATA DA EMISSÃO' in n and not data_emissao:
                            data_emissao = nota[index + 1].strip()
                            print('Data da Emissao' , data_emissao)
                        if 'INSCRIÇÃO ESTADUAL' in n and not descricao:
                            descricao = nota[index + 2].strip()
                            print('Descrição' , descricao)

                    #NOVO PADRAO
                    #numero = nota[97].split(' ')[1].strip()
                    #print('Numero' , numero)
                    #data_emissao = nota[84].strip()
                    #print('Data Emissao' , data_emissao)
                    quantidade = nota[15].strip()
                    print('Quantidade' , quantidade)
                    valor_total_nota = nota[44].strip().replace('.','').replace(',','.')
                    print('Valor Total da Nota' , valor_total_nota)
                    valor_unitario = nota[16].strip().replace('.','').replace(',','.')
                    print('Valor unitario' , valor_unitario)
                    #descricao = nota[111].strip()
                    #print('Descricao' , descricao)
                    #PADRAO ANTERIOR
                    """numero = nota.split('NF-eNº')
                    numero = numero[1].split('SÉRIE')[0].strip()
                    print('Numero', numero)
                    data_emissao = nota.split('NATUREZA DA OPERAÇÃO')
                    data_emissao = data_emissao[1].split(' ')[1]
                    print('Data Emissão' , data_emissao)
                    quantidade = nota.split('ESPÉCIE')
                    quantidade = quantidade[1].split('QUANTIDADE')[0]
                    print('Quantidade' , quantidade)
                    valor_total_nota = nota.split('VALOR TOTAL DOS PRODUTOS')
                    valor_total_nota = valor_total_nota[1].split('0,000,')[0].replace('.','').replace(',','.')
                    print('Valor Total da Nota' , valor_total_nota)
                    valor_unitario = nota.split('KG')
                    valor_unitario = valor_unitario[1].split('.')[0].replace('.','').replace(',','.')
                    print('Valor unitario' , valor_unitario)
                    descricao = nota.split('DESCRIÇÃO DOS PRODUTOS / SERVIÇOS')
                    descricao = descricao[1].split('DADOS DOS PRODUTOS')[0]
                    descricao = descricao.split(',')[2][2:][:-6]
                    print('Descricao' , descricao)"""

            
            nfe = Nfe()
            data_emissao = data_emissao.split('/')
            nfe.data = date(int(data_emissao[2]),int(data_emissao[1]),int(data_emissao[0]))
        
            if Nfe.objects.filter(numero=numero).exists():
                response = JsonResponse({'sucesso':False,'msg':'Documento já inserido.'})
                response.status_code = 400
                return response 
            nfe.numero = numero
            nfe.pdf_dir = uploaded_file_url
            nfe.quantidade = quantidade
            nfe.valor_unitario = valor_unitario
            nfe.valor_total_nota = valor_total_nota
            nfe.descricao = descricao
            servico = Servico.objects.filter(descricao=descricao)
            if not servico.exists():
                response = JsonResponse({'sucesso':False,'msg':'Serviço não catalogado.'})
                response.status_code = 400
                return response
            nfe.servico = servico[0]
            print(nfe.servico)
            nfe.save()
            response = JsonResponse( {'sucesso':True,'msg':'Documento registrado com sucesso.'} )
            response.status_code = 200
            return response 
        except:
            response = JsonResponse( {'sucesso':False,'msg':'Formato do documento inválido.'} )
            response.status_code = 400
            return response

    