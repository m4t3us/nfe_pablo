from django.urls import path, re_path
from .views import NfeView

urlpatterns = [
    path('', NfeView.listTemplate , name='nfe_template_list'),
    path('list', NfeView.list , name='nfe_list'),
    path('lerNfe', NfeView.lerNfe , name='nfe_ler'),
]

