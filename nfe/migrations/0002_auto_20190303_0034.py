# Generated by Django 2.1.7 on 2019-03-03 03:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    dependencies = [
        ('servico', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('nfe', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalNfe',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('numero', models.IntegerField(db_index=True, default=None)),
                ('data', models.DateTimeField(default=None)),
                ('remetente', models.CharField(default=None, max_length=240)),
                ('quantidade', models.CharField(default=None, max_length=140)),
                ('valor_unitario', models.DecimalField(decimal_places=2, default=None, max_digits=50)),
                ('valor_total', models.DecimalField(decimal_places=2, default=None, max_digits=50)),
                ('valor_total_nota', models.DecimalField(decimal_places=2, default=None, max_digits=50)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('servico', models.ForeignKey(blank=True, db_constraint=False, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='servico.Servico')),
            ],
            options={
                'verbose_name': 'historical nfe',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
        migrations.AddField(
            model_name='nfe',
            name='data',
            field=models.DateTimeField(default=None),
        ),
        migrations.AddField(
            model_name='nfe',
            name='numero',
            field=models.IntegerField(default=None, unique=True),
        ),
        migrations.AddField(
            model_name='nfe',
            name='quantidade',
            field=models.CharField(default=None, max_length=140),
        ),
        migrations.AddField(
            model_name='nfe',
            name='remetente',
            field=models.CharField(default=None, max_length=240),
        ),
        migrations.AddField(
            model_name='nfe',
            name='servico',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='servico.Servico'),
        ),
        migrations.AddField(
            model_name='nfe',
            name='valor_total',
            field=models.DecimalField(decimal_places=2, default=None, max_digits=50),
        ),
        migrations.AddField(
            model_name='nfe',
            name='valor_total_nota',
            field=models.DecimalField(decimal_places=2, default=None, max_digits=50),
        ),
        migrations.AddField(
            model_name='nfe',
            name='valor_unitario',
            field=models.DecimalField(decimal_places=2, default=None, max_digits=50),
        ),
    ]
