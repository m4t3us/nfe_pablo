from django.db import models
from simple_history.models import HistoricalRecords


class Obra(models.Model):
    codigo = models.IntegerField(unique=True,db_index=False)
    cidade = models.CharField(max_length=240)
    estado = models.CharField(max_length=140)
  
    history = HistoricalRecords()

    def __str__(self):
        return str(self.codigo)



