from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.db.models import Count
from django.conf import settings
from .models import Obra
import os
from django.contrib.auth.decorators import login_required, permission_required

class ObraView:

    @login_required
    def selecionarObra(request,):
        context = { 'template' : 'Selecione a obra desejada' }
        return render( request , 'selecionar-obra.html' , context )

    @login_required
    def list(request, codigo=None):
        if codigo:
            pass
        else:
            obras = Obra.objects.all().values()
            obras = list(obras)
            return JsonResponse( obras, safe=False  )

  