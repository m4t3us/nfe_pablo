from django.urls import path, re_path
from .views import ObraView

urlpatterns = [
    path('selecionar_obra', ObraView.selecionarObra , name='obra_template'),
    path('list', ObraView.list , name='obra_list'),
]

